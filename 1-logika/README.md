# Построй си компютър

(Konstru komputilo на [Есперанто](https://lernu.net/esperanto))

Това е проектът ми за изграждане на 4-битов компютър от [чипове тип 7400](https://en.wikipedia.org/wiki/List_of_7400-series_integrated_circuits) чипове. Ползвал съм програмите:
- [Parabola GNU+Linux](https://www.parabola.nu/) за операционна система
- [Digital](https://github.com/hneemann/Digital) за симулациите на компютърната логика (инсталирана с пакета [digital](https://aur.archlinux.org/packages/digital) от [AUR](https://aur.archlinux.org/))
- [InkScape](https://inkscape.org/) за чертежите на чиповете в Digital
