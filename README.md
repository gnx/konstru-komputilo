# Изграждане на 4-битов компютър

За изграждането му ползвах като ориентир [видеата на Ben Eater](https://www.youtube.com/watch?v=HyznrdDSSGM&list=PLowKtXNTBypGqImE405J2565dvjafglHU), макар че направих множество промени спрямо неговия компютър.

![Схема на завършения компютър](computer-phisical.svg)